package com.example.demo.processor;

import com.example.demo.dto.Message;
import com.example.demo.exception.MessageLengthNotValidException;
import com.example.demo.exception.InvalidMobileNumberException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("messageProcessor")
public class MessageProcessor implements Processor {


    public MessageProcessor() {

    }

    @Override
    public void process(Exchange exchange) throws Exception{

        String pattern = "^7|0|(?:\\\\+94)[0-9]{9,10}$";
        Pattern r = Pattern.compile(pattern);

        Message msg = exchange.getIn().getBody(Message.class);
        Matcher matcher = r.matcher(String.valueOf(msg.getSender()));

        if(msg.getSender() < 0){
            throw new InvalidMobileNumberException("Mobile number should be positive!");
        }
        if(msg.getMessageBody().length()>50 || msg.getMessageBody().length()<2){
            throw new MessageLengthNotValidException("Message length is not valid!");
        }


    }
}
