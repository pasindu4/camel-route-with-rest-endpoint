package com.example.demo;

import com.example.demo.dto.Message;
import com.example.demo.exception.MessageLengthNotValidException;
import com.example.demo.exception.InvalidMobileNumberException;
import com.example.demo.processor.MessageProcessor;
import com.example.demo.service.MessageService;
import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class SmsNotificationRouter extends RouteBuilder {

	private final MessageService messageService;
	@BeanInject
	private MessageProcessor messageProcessor;

	public SmsNotificationRouter(MessageService messageService) {
		this.messageService = messageService;
	}

	@Override
	public void configure() throws Exception {

		restConfiguration().component("servlet").bindingMode(RestBindingMode.json).clientRequestValidation(true);

		rest("/message")
				.post("/add").consumes(MediaType.APPLICATION_JSON_VALUE).type(Message.class).outType(Message.class)
				.to("seda:newMessage");


		from("seda:newMessage").errorHandler(deadLetterChannel("log:DLC")
				.maximumRedeliveries(5)
				.backOffMultiplier(2)
				.retryAttemptedLogLevel(LoggingLevel.WARN))
				.routeId("smpp-sender").doTry().process(messageProcessor)
				.setHeader("CamelSmppDestAddr",simple("94${in.body.sender}"))
				.setBody(simple("${in.body.messageBody}"))
				.doCatch(InvalidMobileNumberException.class)
				.process(new MessageProcessor() {
					public void process(Exchange exchange) throws Exception {
						        exchange.getIn().setBody(simple("Invalid Mobile Number!"));
        						exchange.getIn().setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        						exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 400);
					}
				}).doCatch(MessageLengthNotValidException.class).process(new MessageProcessor() {
			public void process(Exchange exchange) throws Exception {

				exchange.getIn().setBody("Message length is not valid!");
				exchange.getIn().setHeader(Exchange.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
				exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 400);
			}
		}).log("Received body ")

				.to("smpp://{{smpp.tr.systemid}}@{{smpp.tr.host}}:{{smpp.tr.port}}?password={{smpp.tr.password}}&enquireLinkTimer=3000&synchronous=true&transactionTimer=5000&reconnectDelay=3000&sourceAddrTon={{smpp.source.addr.ton}}&sourceAddrNpi={{smpp.source.addr.npi}}&destAddrTon={{smpp.dest.addr.ton}}&destAddrNpi={{smpp.dest.addr.npi}}&sourceAddr={{smpp.source.address}}");


	}
}