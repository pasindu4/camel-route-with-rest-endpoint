package com.example.demo.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Message {

    private int id;

    @NotNull(message = "sender can not be null")
    @Pattern(regexp = "^7|0|(?:\\\\+94)[0-9]{9,10}$", message = "must contain 9 digits")
    private long sender;

    @Size(min = 5, max = 50, message = "message size must be between 5 and 50")
    private String messageBody;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public void setSender(long sender){
        this.sender = sender;
    }
    public long getSender(){
        return sender;
    }

    public void setMessageBody(String messageBody){
        this.messageBody = messageBody;
    }
    public String getMessageBody(){
        return messageBody;
    }

    public String toString(){
        return "sender "+ sender + "messageBody " + messageBody;
    }
}
