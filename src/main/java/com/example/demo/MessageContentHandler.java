package com.example.demo;

import org.apache.camel.Body;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component("messageContentHandler")
public class MessageContentHandler {

	private MessageSource messageSource;

	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;

	}

	public String getContent(@Body NotificationProvider model) {

		return model.getContent(messageSource);

	}
}

