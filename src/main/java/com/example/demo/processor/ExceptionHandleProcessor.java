package com.example.demo.processor;

import com.example.demo.exception.CamelCustomException;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ExceptionHandleProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        System.out.println("Exception Thrown");
        throw new CamelCustomException();
    }
}
