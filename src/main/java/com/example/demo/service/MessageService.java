package com.example.demo.service;

import com.example.demo.dto.Message;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    private List<Message> list = new ArrayList<>();

    public Message addMessage(Message message){
        list.add(message);
        return message;
    }

    public List<Message>getMessages(){
        return list;
    }
}
