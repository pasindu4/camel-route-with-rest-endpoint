package com.example.demo.exception;

public class MessageLengthNotValidException extends Exception {

    public MessageLengthNotValidException(String errorMessage) {
        super(errorMessage);
    }
}
