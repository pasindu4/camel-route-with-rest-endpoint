package com.example.demo.exception;

public class InvalidMobileNumberException extends Exception{

    public InvalidMobileNumberException(String errorMessage) {
        super(errorMessage);
    }
}
